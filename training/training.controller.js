const express = require('express');
const router = express.Router();
const trainingService = require('./training.service');


router.post('/uploadExcel', uploadExcel);


module.exports = router;


function uploadExcel(req, res, next) {

    trainingService.uploadExcel(req)
        .then((message) => {
            res.json(message);
        })
        .catch(next);
}