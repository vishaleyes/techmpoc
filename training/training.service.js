const config = require('config.json');
const xlsx = require("node-xlsx");

const db = require('_helpers/db');

module.exports = {
  uploadExcel,
}

async function uploadExcel(req) {
  console.log("files -- ", req.files);
  var filePath = req.files.filePath;
  console.log("File path----", filePath);

  if (!filePath.name.includes(".xls"))
    return reject("Import file type is invalid");

  const workSheetsFromFile = xlsx.parse(filePath.data, { cellDates: true });
  var data = [];
  if (workSheetsFromFile.length > 0)
    data = workSheetsFromFile[0].data;

  console.log("Sheet Data", data);
  if (
    !data[0].includes("Training Name") ||
    !data[0].includes("Training Type") ||
    //!data[0].includes("ZOOM UUID") ||
    //!data[0].includes("Zoom Meeting Type") ||
    !data[0].includes("Training Start Date") ||
    !data[0].includes("Training End Date") ||
    !data[0].includes("Nomination End Date")

  )
    return reject("Import data not matched please provide valid file");
  //console.log("Data------------------", data);
  data[0] = [
    "trainingName",
    "trainingType",
    "trainingStartDate",
    "trainingEndDate",
    "trainingPrequisites",
    "nominationEndDate"
  ];
  console.log("Read Sheet Data------------------");
  //READ DATA FROM SHEET
  var dataDictArray = [];
  data.map((each, rowIndex) => {
    var dict = {};
    each.map(async (key, columnIndex) => {
      if (rowIndex != 0) {
        dict[data[0][columnIndex]] = data[rowIndex][columnIndex + 1];
      }
    });
    if (rowIndex != 0) dataDictArray.push(dict);
  });
  let [error, result] = await to(db.Training.bulkCreate(dataDictArray));

  if (error)
    return reject(error)

  return ({ message: "Data uploaded successfully..." });

}