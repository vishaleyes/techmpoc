const { DataTypes } = require('sequelize');

module.exports = model;

function model(sequelize) {
    const attributes = {
        trainingName: { type: DataTypes.STRING, allowNull: false },
        trainingType: { type: DataTypes.STRING, allowNull: false },
        trainingStartDate: { type: DataTypes.STRING, allowNull: false },
        trainingEndDate: { type: DataTypes.STRING, allowNull: false },
        trainingPrequisites: { type: DataTypes.STRING },
        nominationEndDate: { type: DataTypes.STRING },
        created: { type: DataTypes.DATE, allowNull: false, defaultValue: DataTypes.NOW },
        updated: { type: DataTypes.DATE }
    }

    const options = {
        // disable default timestamp fields (createdAt and updatedAt)
        timestamps: false,
    };
    return sequelize.define('training', attributes, options);
}



