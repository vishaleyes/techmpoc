const { DataTypes } = require('sequelize');

module.exports = model;

function model(sequelize) {
    const attributes = {
        userId: { type: DataTypes.INTEGER, allowNull: false },
        monthCalendar: { type: DataTypes.STRING, allowNull: true },
        responseType: { type: DataTypes.STRING, allowNull: true },
        clientName: { type: DataTypes.STRING, allowNull: true },
        status: { type: DataTypes.STRING, allowNull: true },
        stage: { type: DataTypes.STRING, allowNull: true },
        dealType: { type: DataTypes.STRING, allowNull: true },
        dealTerm: { type: DataTypes.STRING, allowNull: true },
        rfpReceivedDate: { type: DataTypes.STRING, allowNull: true },
        rfpRespondedDate: { type: DataTypes.STRING, allowNull: true },
        geo: { type: DataTypes.STRING, allowNull: true },
        location: { type: DataTypes.STRING, allowNull: true },
        geoLeads: { type: DataTypes.STRING, allowNull: true },
        deliveryHeads: { type: DataTypes.STRING, allowNull: true },
        btsMoLead: { type: DataTypes.STRING, allowNull: true },
        salesLead: { type: DataTypes.STRING, allowNull: true },
        programLead: { type: DataTypes.STRING, allowNull: true },
        technicalLead: { type: DataTypes.STRING, allowNull: true },
        deliveryLedBy: { type: DataTypes.STRING, allowNull: true },
        automationType: { type: DataTypes.STRING, allowNull: true },
        rpaPlatform: { type: DataTypes.STRING, allowNull: true },
        developmentResource: { type: DataTypes.STRING, allowNull: true },
        supportResource: { type: DataTypes.STRING, allowNull: true },
        totalResource: { type: DataTypes.STRING, allowNull: true },
        currency: { type: DataTypes.STRING, allowNull: true },
        margin: { type: DataTypes.STRING, allowNull: true },
        totalPricing: { type: DataTypes.STRING, allowNull: true },
        totalCashValue: { type: DataTypes.STRING, allowNull: true },
        ebitda: { type: DataTypes.STRING, allowNull: true },
        consideration: { type: DataTypes.STRING, allowNull: true },
        benefits: { type: DataTypes.STRING, allowNull: true },
        serverUsage: { type: DataTypes.STRING, allowNull: true },
        platform: { type: DataTypes.STRING, allowNull: true },
        projectStartDate: { type: DataTypes.STRING, allowNull: true },
        projectEndDate: { type: DataTypes.STRING, allowNull: true },
        developmentDuration: { type: DataTypes.STRING, allowNull: true },
        actualCashValue: { type: DataTypes.STRING, allowNull: true },
        created: { type: DataTypes.DATE, allowNull: false, defaultValue: DataTypes.NOW },
        updated: { type: DataTypes.DATE },
    };

    const options = {
        // enable default timestamp fields (createdAt and updatedAt)
        timestamps: true     
    };

    return sequelize.define('rfcForm', attributes, options);
}

/*
monthCalendar,
responseType,
clientName,
status,
stage,
dealType,
dealTerm,
rfpReceivedDate,
rfpRespondedDate,
geo,
location,
geoLeads,
deliveryHeads,
btsMoLead,
salesLead,
programLead,
technicalLead,
deliveryLedBy,
automationType,
rpaPlatform,
developmentResource,
supportResource,
totalResource,
currency,
margin,
totalPricing,
totalCashValue,
ebitda,
consideration,
benefits,
serverUsage,
platform,
projectStartDate,
projectEndDate,
developmentDuration,
actualCashValue
*/