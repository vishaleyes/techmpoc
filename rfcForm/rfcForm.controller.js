﻿const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('_middleware/validate-request');
const authorize = require('_middleware/authorize')
const Role = require('_helpers/role');
const rfcFormService = require('./rfcForm.service');

router.get('/', authorize([Role.Admin, Role.Manager, Role.User]), getAll);
router.get('/:id', authorize(), getById);
router.post('/', authorize([Role.Admin, Role.Manager, Role.User]), createSchema, create);
router.put('/:id', authorize(), updateSchema, update);
router.delete('/:id', authorize(), _delete);

module.exports = router;

function getAll(req, res, next) {
    if (req.user.role === Role.Admin || req.user.role === Role.Manager) {
        rfcFormService.getAll()
            .then(rfcForms => res.json(rfcForms))
            .catch(next);
    }
    else {
        rfcFormService.getAll(req.user.id)
            .then(rfcForms => res.json(rfcForms))
            .catch(next);
    }
}

function getById(req, res, next) {
    console.log("getById", req.user);
    // users can get their own rfcForm and admins can get any rfcForm
    if (Number(req.params.id) !== req.user.id && req.user.role !== Role.Admin && req.user.role !== Role.Manager) {
        return res.status(401).json({ message: 'Unauthorized' });
    }

    rfcFormService.getById(req.params.id)
        .then(rfcForm => rfcForm ? res.json(rfcForm) : res.sendStatus(404))
        .catch(next);
}

function createSchema(req, res, next) {
    const schema = Joi.object({
        userId: Joi.number().required(),
        monthCalendar: Joi.string(),
        responseType: Joi.string(),
        clientName: Joi.string(),
        status: Joi.string(),
        stage: Joi.string(),
        dealType: Joi.string(),
        dealTerm: Joi.string(),
        rfpReceivedDate: Joi.string(),
        rfpRespondedDate: Joi.string(),
        geo: Joi.string(),
        location: Joi.string(),
        geoLeads: Joi.string(),
        deliveryHeads: Joi.string(),
        btsMoLead: Joi.string(),
        salesLead: Joi.string(),
        programLead: Joi.string(),
        technicalLead: Joi.string(),
        deliveryLedBy: Joi.string(),
        automationType: Joi.string(),
        rpaPlatform: Joi.string(),
        developmentResource: Joi.string(),
        supportResource: Joi.string(),
        totalResource: Joi.string(),
        currency: Joi.string(),
        margin: Joi.string(),
        totalPricing: Joi.string(),
        totalCashValue: Joi.string(),
        ebitda: Joi.string(),
        consideration: Joi.string(),
        benefits: Joi.string(),
        serverUsage: Joi.string(),
        platform: Joi.string(),
        projectStartDate: Joi.string(),
        projectEndDate: Joi.string(),
        developmentDuration: Joi.string(),
        actualCashValue: Joi.string(),
    });
    validateRequest(req, next, schema);
}

function create(req, res, next) {
    rfcFormService.create(req.body)
        .then(rfcForm => res.json(rfcForm))
        .catch(next);
}

function updateSchema(req, res, next) {
    const schemaRules = {
        userId: Joi.number().required(),
        monthCalendar: Joi.string(),
        responseType: Joi.string(),
        clientName: Joi.string(),
        status: Joi.string(),
        stage: Joi.string(),
        dealType: Joi.string(),
        dealTerm: Joi.string(),
        rfpReceivedDate: Joi.string(),
        rfpRespondedDate: Joi.string(),
        geo: Joi.string(),
        location: Joi.string(),
        geoLeads: Joi.string(),
        deliveryHeads: Joi.string(),
        btsMoLead: Joi.string(),
        salesLead: Joi.string(),
        programLead: Joi.string(),
        technicalLead: Joi.string(),
        deliveryLedBy: Joi.string(),
        automationType: Joi.string(),
        rpaPlatform: Joi.string(),
        developmentResource: Joi.string(),
        supportResource: Joi.string(),
        totalResource: Joi.string(),
        currency: Joi.string(),
        margin: Joi.string(),
        totalPricing: Joi.string(),
        totalCashValue: Joi.string(),
        ebitda: Joi.string(),
        consideration: Joi.string(),
        benefits: Joi.string(),
        serverUsage: Joi.string(),
        platform: Joi.string(),
        projectStartDate: Joi.string(),
        projectEndDate: Joi.string(),
        developmentDuration: Joi.string(),
        actualCashValue: Joi.string(),
    };

    // only admins can update role
    if (req.user.role === Role.Admin) {
        schemaRules.role = Joi.string().valid(Role.Admin, Role.User).empty('');
    }
    const schema = Joi.object(schemaRules)
    validateRequest(req, next, schema);
}

function update(req, res, next) {
    // users can update their own rfcForm and admins can update any rfcForm
    if (Number(req.params.id) !== req.user.id && req.user.role !== Role.Admin && req.user.role !== Role.Manager) {
        return res.status(401).json({ message: 'Unauthorized' });
    }

    rfcFormService.update(req.params.id, req.body)
        .then(rfcForm => res.json(rfcForm))
        .catch(next);
}

function _delete(req, res, next) {
    console.log("_delete", req.user);
    // users can delete their own rfcForm and admins can delete any rfcForm
    // if (Number(req.params.id) !== req.user.id && req.user.role !== Role.Admin && req.user.role !== Role.Manager) {
    //     return res.status(401).json({ message: 'Unauthorized' });
    // }

    rfcFormService.delete(req.params.id)
        .then(() => res.json({ message: 'RfcForm deleted successfully' }))
        .catch(next);
}
