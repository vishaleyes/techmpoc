﻿
const sendEmail = require('_helpers/send-email');
const db = require('_helpers/db');

module.exports = {
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function getAll(userId = null) {
    console.log("userId ==", userId);
    if (userId == null) {
        const rfcForms = await db.RfcForm.findAll();
        return rfcForms.map(x => basicDetails(x));
    }
    else {
        const rfcForms = await db.RfcForm.findAll({
            where: { userId: userId }
        });
        return rfcForms.map(x => basicDetails(x));
    }
}

async function getById(id) {
    const rfcForm = await getRfcForm(id);
    return basicDetails(rfcForm);
}

async function create(params) {
    // validate
    if (!params.userId) {
        throw 'Required User Id';
    }
    const rfcForm = new db.RfcForm(params);
    // save rfcForm
    await rfcForm.save();

    return basicDetails(rfcForm);
}

async function update(id, params) {
    const rfcForm = await getRfcForm(id);

    // copy params to rfcForm and save
    Object.assign(rfcForm, params);
    rfcForm.updated = Date.now();
    await rfcForm.save();

    return basicDetails(rfcForm);
}

async function _delete(id) {
    const rfcForm = await getRfcForm(id);
    await rfcForm.destroy();
}

// helper functions
async function getRfcForm(id) {
    const rfcForm = await db.RfcForm.findByPk(id);
    if (!rfcForm) throw 'RfcForm not found';
    return rfcForm;
}

function basicDetails(rfcForm) {
    const { id, userId, monthCalendar, responseType, clientName, status, stage, dealType, dealTerm, rfpReceivedDate, rfpRespondedDate, geo, location, geoLeads, deliveryHeads, btsMoLead, salesLead, programLead, technicalLead, deliveryLedBy, automationType, rpaPlatform, developmentResource, supportResource, totalResource, currency, margin, totalPricing, totalCashValue, ebitda, consideration, benefits, serverUsage, platform, projectStartDate, projectEndDate, developmentDuration, actualCashValue } = rfcForm;
    return { id, userId, monthCalendar, responseType, clientName, status, stage, dealType, dealTerm, rfpReceivedDate, rfpRespondedDate, geo, location, geoLeads, deliveryHeads, btsMoLead, salesLead, programLead, technicalLead, deliveryLedBy, automationType, rpaPlatform, developmentResource, supportResource, totalResource, currency, margin, totalPricing, totalCashValue, ebitda, consideration, benefits, serverUsage, platform, projectStartDate, projectEndDate, developmentDuration, actualCashValue };
}

async function sendEmailToUser(rfcForm, origin) {
    let message;
    if (origin) {
        const verifyUrl = `${origin}/rfcForm/verify-email?token=${rfcForm.verificationToken}`;
        message = `<p>Please click the below link to verify your email address:</p>
                   <p><a href="${verifyUrl}">${verifyUrl}</a></p>`;
    } else {
        message = `<p>Please use the below token to verify your email address with the <code>/rfcForm/verify-email</code> api route:</p>
                   <p><code>${rfcForm.verificationToken}</code></p>`;
    }

    await sendEmail({
        to: rfcForm.email,
        subject: 'Sign-up Verification API - Verify Email',
        html: `<h4>Verify Email</h4>
               <p>Thanks for registering!</p>
               ${message}`
    });
}
